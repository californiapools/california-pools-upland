California Pools is one of the largest pool builders in the country and brings award-winning custom pools to the Upland area. As seen on Pool Kings, we offer expert design solutions and innovative construction techniques to provide our customers with the highest-quality pool or backyard anywhere.

Address: 9757 7th St, Suite 809, Rancho Cucamonga, CA 91730, USA

Phone: 909-890-9907

Website: https://californiapools.com/locations/upland
